export class UserEntity {
  id: string
  login: string
  passwordHash: string
  recoveryToken: string
  entityId: string
}
